﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.Models
{
    public class Address
    {
        [Key]
        [Required]
        public long Id { get; set; }
        [Required]
        public String street { get; set; }
        [Required]
        public String number { get; set; }
        [Required]
        //[MaxLength(7),MinLength(7)]
        public String zipcode { get; set; }
        [Required]
        public String town { get; set; }
        [Required]
        public User User { get; set; }
        [Required]
        public long UserId { get; set; }
        
       

        public Address(){}

        public Address(string street, string number, string zipcode, string town)
        {
            this.street = street ?? throw new ArgumentNullException(nameof(street));
            this.number = number ?? throw new ArgumentNullException(nameof(number));
            this.zipcode = zipcode ?? throw new ArgumentNullException(nameof(zipcode));
            this.town = town ?? throw new ArgumentNullException(nameof(town));
        }

        public void UpdateAddress(string street, string number, string zipcode, string town)
        {
            this.street = street;
            this.number = number;
            this.zipcode = zipcode;
            this.town = town;
        }




    }
}
