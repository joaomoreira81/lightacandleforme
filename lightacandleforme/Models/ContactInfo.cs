﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.Models
{
    public class ContactInfo
    {
        [Key, Required]
        public long Id { get; set; }
        [Required, RegularExpression(@"/ ^\S +@\S +\.\S +$/")]
        public string email { get; set; }
        [Required, MinLength(9), MaxLength(9)]
        public string phone_number { get; set; }
        [Required]
        public long UserId { get; set; }
        [Required]
        public User User { get; set; }


        public ContactInfo() { }
        
        public ContactInfo(string email, string phone_number)
        {
            this.email = email ?? throw new ArgumentNullException(nameof(email));
            this.phone_number = phone_number ?? throw new ArgumentNullException(nameof(phone_number));
        }

        public void updateEmail(string email)
        {
            this.email = email;
        }
        public void updatePhoneNumber(string phone_number)
        {
            this.phone_number = phone_number;
        }
    }
}
