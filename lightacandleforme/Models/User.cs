﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.Models
{
    public class User
    {

        [Key]
        [Required]
        public long Id { get; set; }
        [Required]
        public Address Address { get; set; }
        [Required]
        public ContactInfo Contact_info { get; set; }
        [Required]
        public String name { get; set; }
        [Required, MinLength(9), MaxLength(9)]
        public String nif { get; set; }
        [Required]
        public DateTime dateofbirth { get; set; }

        public User() { }

        public User(Address address, ContactInfo contact_info, string name, string nif, DateTime dateofbirth)
        {
            this.Address = address;
            this.Contact_info = contact_info;
            this.name = name;
            this.nif = nif;
            this.dateofbirth = dateofbirth;
        }

        public void UpdateUser(string name, string nif, DateTime dateofbirth)
        {
            this.name = name;
            this.nif = nif;
            this.dateofbirth = dateofbirth;
        }
    }
}
