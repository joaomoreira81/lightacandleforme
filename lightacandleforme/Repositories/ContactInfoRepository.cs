﻿using AutoMapper;
using lightacandleforme.Data;
using lightacandleforme.DTOs;
using lightacandleforme.Factories;
using lightacandleforme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.Repositories
{
    public class ContactInfoRepository : IRepository<ContactInfoDTO>
    {

        private readonly ProjectContext _context;
        private readonly ContactInfoFactory factory;

        public ContactInfoRepository(ProjectContext context)
        {
            _context = context;
            factory = new ContactInfoFactory();
        }
        public ContactInfoDTO CreateNew(ContactInfoDTO dto)
        {
            ContactInfo c = factory.fromDTO(dto);
            var saved = _context.ContactInfos.Add(c);
            _context.SaveChanges();
            return factory.ToDto(saved.Entity);
        }

        public void DeleteWithId(long id)
        {
            ContactInfo c = _context.ContactInfos.Find(id);
            _context.ContactInfos.Remove(c);
            _context.SaveChanges();
        }
        public void DeleteWithUserId(long id)
        {
            var info = _context.ContactInfos.Where(a => a.UserId == id);
            _context.ContactInfos.Remove((ContactInfo)info);
            _context.SaveChanges();
        }

        public ContactInfo GetWithUserId(long id)
        {
            var info = _context.ContactInfos.Where(a => a.UserId == id);
            _context.SaveChanges();
            return (ContactInfo)info;
        }

        public List<ContactInfoDTO> GetAll()
        {
            List<ContactInfoDTO> dtos = new List<ContactInfoDTO>();

            var infos = _context.ContactInfos;

            foreach(ContactInfo contactinfo in infos)
            {
                dtos.Add(factory.ToDto(contactinfo));
            }
            return dtos;
                
        }

        public ContactInfoDTO GetWithId(long id)
        {
            ContactInfo a = _context.ContactInfos.Find(id);
            return factory.ToDto(a);
        }

        public void UpdateAllFieldsWithId(long id, ContactInfoDTO dto)
        {
            throw new NotImplementedException();
        }

        public void UpdateNonNullFieldsWithId(long id, ContactInfoDTO dto)
        {
            throw new NotImplementedException();
        }
    }
}
