﻿using AutoMapper;
using lightacandleforme.Data;
using lightacandleforme.DTOs;
using lightacandleforme.Factories;
using lightacandleforme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.Repositories
{
    public class UserRepository : IRepository<UserDTO>
    {

        private readonly ProjectContext _context;
        private readonly UserFactory factory;

        public UserRepository(ProjectContext context)
        {
            _context = context;
            factory = new UserFactory();
        }
        public UserDTO CreateNew(UserDTO dto)
        {
            /*
            Console.WriteLine("No repo");
            Address a = new Address("rua", "90", "1234567", "coiso");
            Console.WriteLine("No repo - add");
            ContactInfo c = new ContactInfo("adad@asdsa.pt", "123456789");
            Console.WriteLine("No repo- contact");
            User u = new User(a, c, "Nome", "123456789", new DateTime(2000, 01, 01, 0, 0, 0));
            Console.WriteLine("No repo - user");
            var saved = _context.Users.Add(u);
            _context.SaveChanges();
            return 
                
                factory.ToDto(saved.Entity);
            */
            Console.WriteLine(dto.name);
            Console.WriteLine(dto.contact_infodto.email);
            Console.WriteLine(dto.addressdto.street);
            User user = factory.fromDTO(dto);
            _context.Users.Add(user);
            _context.SaveChanges();
            return factory.ToDto(user);
        }

        public void DeleteWithId(long id)
        {
            var user = _context.Users.Find(id);
            _context.Users.Remove(user);
            _context.SaveChanges();
        }

        public List<UserDTO> GetAll()
        {
            throw new NotImplementedException();
        }

        public UserDTO GetWithId(long id)
        {
            throw new NotImplementedException();
        }

        public void UpdateAllFieldsWithId(long id, UserDTO dto)
        {
            throw new NotImplementedException();
        }

        public void UpdateNonNullFieldsWithId(long id, UserDTO dto)
        {
            throw new NotImplementedException();
        }
    }
}
