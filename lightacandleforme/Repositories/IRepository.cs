﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.Repositories
{
    interface IRepository<DTO>
    {
        List<DTO> GetAll();
        DTO CreateNew(DTO dto);
        DTO GetWithId(long id);
        void UpdateAllFieldsWithId(long id, DTO dto);
        void UpdateNonNullFieldsWithId(long id, DTO dto);
        void DeleteWithId(long id);
    }
}
