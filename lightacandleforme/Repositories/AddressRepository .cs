﻿using AutoMapper;
using lightacandleforme.Data;
using lightacandleforme.DTOs;
using lightacandleforme.Factories;
using lightacandleforme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.Repositories
{
    public class AddressRepository : IRepository<AddressDTO>
    {

        private readonly ProjectContext _context;
        private readonly AddressFactory factory;

        public AddressRepository(ProjectContext context)
        {
            _context = context;
            factory = new AddressFactory();
        }
        public AddressDTO CreateNew(AddressDTO dto)
        {
            Address a = factory.fromDTO(dto);
            var saved = _context.Addresses.Add(a);
            _context.SaveChanges();
            return factory.ToDto(saved.Entity);
        }

        public void DeleteWithId(long id)
        {
            Address a = _context.Addresses.Find(id);
            _context.Addresses.Remove(a);
            _context.SaveChanges();
        }
        public void DeleteWithUserId(long id)
        {
            var add = _context.Addresses.Where(a => a.UserId == id);
            _context.Addresses.Remove((Address)add);
            _context.SaveChanges();
        }

        public Address GetWithUserId(long id)
        {
            var add = _context.Addresses.Where(a => a.UserId == id);
            _context.SaveChanges();
            return (Address)add;
        }

        public List<AddressDTO> GetAll()
        {
            List<AddressDTO> dtos = new List<AddressDTO>();

            var addresses = _context.Addresses;

            foreach(Address address in addresses)
            {
                dtos.Add(factory.ToDto(address));
            }
            return dtos;
                
        }

        public AddressDTO GetWithId(long id)
        {
            Address a = _context.Addresses.Find(id);
            return factory.ToDto(a);
        }

        public void UpdateAllFieldsWithId(long id, AddressDTO dto)
        {
            throw new NotImplementedException();
        }

        public void UpdateNonNullFieldsWithId(long id, AddressDTO dto)
        {
            throw new NotImplementedException();
        }

       
    }
}
