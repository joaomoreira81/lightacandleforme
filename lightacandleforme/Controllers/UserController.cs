﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using lightacandleforme.Data;
using lightacandleforme.DTOs;
using lightacandleforme.Repositories;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace lightacandleforme.Controllers
{
    [ApiController]
    [EnableCors("AllowAll")]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {

        private readonly UserRepository _repo;
        private readonly ProjectContext _context;

        private readonly ILogger<UserController> _logger;

        public UserController(ILogger<UserController> logger, ProjectContext context)
        {
            _logger = logger;
            _context = context;
            _repo = new UserRepository(_context);
        }

        [HttpGet]
        public IEnumerable<UserDTO> Get()
        {
            return null;
        }

        [HttpPost]
        public UserDTO CreateNew(UserDTO user)
        {
            Console.WriteLine("No controler");
            return _repo.CreateNew(user);
        }

        [HttpDelete("{id}", Name = "DeleteUser")]
        public void Delete(int id)
        {
            _repo.DeleteWithId(id);
        }
    }
}
