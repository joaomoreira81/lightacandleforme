﻿using lightacandleforme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.DTOs
{
    public class UserDTO 
    {
        public long Id { get; set; }
        public AddressDTO addressdto { get; set; }
        public ContactInfoDTO contact_infodto { get; set; }
        public String name { get; set; }
        public String nif { get; set; }
        public DateTime dateofbirth { get; set; }

        public UserDTO() { }
        public UserDTO(long Id, AddressDTO addressdto, ContactInfoDTO contact_infodto, string name, string nif, DateTime dateofbirth)
        {
            this.Id = Id;
            this.addressdto = addressdto;
            this.contact_infodto = contact_infodto;
            this.name = name;
            this.nif = nif;
            this.dateofbirth = dateofbirth;
        }

        public UserDTO(AddressDTO addressdto, ContactInfoDTO contact_infodto, string name, string nif, DateTime dateofbirth)
        {
            this.addressdto = addressdto;
            this.contact_infodto = contact_infodto;
            this.name = name;
            this.nif = nif;
            this.dateofbirth = dateofbirth;
        }

        public UserDTO(long Id, string name, string nif, DateTime dateofbirth)
        {
            this.Id = Id;
            this.name = name;
            this.nif = nif;
            this.dateofbirth = dateofbirth;
        }


    }
}
