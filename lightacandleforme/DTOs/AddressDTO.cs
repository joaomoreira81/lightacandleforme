﻿using lightacandleforme.Models;
using System;


namespace lightacandleforme.DTOs
{
    public class AddressDTO 
    {

     
            public long Id { get; set; }
            public String street { get; set; }
            public String number { get; set; }
            public String zipcode { get; set; }
            public String town { get; set; }

        public AddressDTO() { }

            public AddressDTO(long Id,string street, string number, string zipcode, string town)
            {
                this.Id = Id;
                this.street = street ?? throw new ArgumentNullException(nameof(street));
                this.number = number ?? throw new ArgumentNullException(nameof(number));
                this.zipcode = zipcode ?? throw new ArgumentNullException(nameof(zipcode));
                this.town = town ?? throw new ArgumentNullException(nameof(town));
            }

        public AddressDTO(string street, string number, string zipcode, string town)
        {
            this.street = street ?? throw new ArgumentNullException(nameof(street));
            this.number = number ?? throw new ArgumentNullException(nameof(number));
            this.zipcode = zipcode ?? throw new ArgumentNullException(nameof(zipcode));
            this.town = town ?? throw new ArgumentNullException(nameof(town));
        }

    }
    }

