﻿using lightacandleforme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.DTOs
{
    public class ContactInfoDTO
    {

        public long Id { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }

        public ContactInfoDTO() { }
        public ContactInfoDTO(long Id, string email, string phone_number)
        {
            this.Id = Id;
            this.email = email ?? throw new ArgumentNullException(nameof(email));
            this.phone_number = phone_number ?? throw new ArgumentNullException(nameof(phone_number));
        }

        public ContactInfoDTO(string email, string phone_number)
        {
            this.email = email ?? throw new ArgumentNullException(nameof(email));
            this.phone_number = phone_number ?? throw new ArgumentNullException(nameof(phone_number));
        }
    }
}
