﻿using lightacandleforme.DTOs;
using lightacandleforme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.Factories
{
    public class UserFactory : IDTO<User, UserDTO>
    {
        private readonly AddressFactory _address;
        private readonly ContactInfoFactory _contact;

        public UserFactory()
        {
            Address address = new Address();
            ContactInfo contact = new ContactInfo();
            _address = new AddressFactory();
            _contact = new ContactInfoFactory();
        }

        public User fromDTO(UserDTO dto)
        {
            Address add = _address.fromDTO(dto.addressdto);
            ContactInfo contact = _contact.fromDTO(dto.contact_infodto);
            return new User(add, contact, dto.name, dto.nif, dto.dateofbirth);

        }

        public UserDTO ToDto(User entity)
        {
            return new UserDTO(entity.Id, entity.name, entity.nif, entity.dateofbirth);
        }
    }
}
