﻿using lightacandleforme.DTOs;
using lightacandleforme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.Factories
{
    public class ContactInfoFactory : IDTO<ContactInfo, ContactInfoDTO>
    {
        public ContactInfo fromDTO(ContactInfoDTO dto)
        {
            return new ContactInfo(dto.email, dto.phone_number);
        }

        public ContactInfoDTO ToDto(ContactInfo entity)
        {
            return new ContactInfoDTO(entity.Id, entity.email, entity.phone_number);
        }
    }
}
