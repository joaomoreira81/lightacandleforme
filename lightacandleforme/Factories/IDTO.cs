﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.Factories
{
    interface IDTO<T, DTO>
    {
        T fromDTO(DTO dto);
        DTO ToDto(T entity);
    }
}
