﻿using lightacandleforme.DTOs;
using lightacandleforme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lightacandleforme.Factories
{
    public class AddressFactory : IDTO<Address, AddressDTO>
    {
        public Address fromDTO(AddressDTO dto)
        {
            Console.WriteLine(dto.street + "fromdtoAddress");
            return new Address(dto.street, dto.number, dto.zipcode, dto.town);
        }

        public AddressDTO ToDto(Address address)
        {
            return new AddressDTO(address.Id, address.street, address.number, address.zipcode, address.town);
        }
    }
}
